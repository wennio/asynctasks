package br.com.samples.estadosdobrasil;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import br.com.samples.estadosdobrasil.util.NetworkUtil;

public class MainActivity extends AppCompatActivity {

    final String TAG = "MainActivity";

    ProgressBar progressBarLoading;
    TextView tvTextoExibido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvTextoExibido = findViewById(R.id.tv_texto_exibido);
        progressBarLoading = findViewById(R.id.pb_loading);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        getMenuInflater().inflate(R.menu.main, m);
        return super.onCreateOptionsMenu(m);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_web_service:
                callWebService();
                break;
            case R.id.menu_clear:
                clearText();
                break;
            case R.id.menu_evo:
                callWebServiceEvo();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void callWebServiceEvo() {
        URL url = NetworkUtil.buildUrlCredenciadasEvo();
        MinhaAsyncTask task = new MinhaAsyncTask();
        task.execute(url);
    }

    public void callWebService() {
        Log.d(TAG, "method callWebService");
        URL url = NetworkUtil.buildUrl("stf");
        MinhaAsyncTask task = new MinhaAsyncTask();
        task.execute(url);
    }

    public void mostrarLoading(){
        tvTextoExibido.setVisibility(View.GONE);
        progressBarLoading.setVisibility(View.VISIBLE);
    }

    public void esconderLoading(){
        tvTextoExibido.setVisibility(View.VISIBLE);
        progressBarLoading.setVisibility(View.GONE);
    }
    public void clearText() {
        Log.d(TAG, "method clearText");
        tvTextoExibido.setText("");
    }


    class MinhaAsyncTask extends AsyncTask<URL, Void, String> {

        @Override
        protected String doInBackground(URL... urls) {
            URL url = urls[0];
            Log.d(TAG, "url utilizada: " + url.toString());
            String json = null;
            try {
                json = NetworkUtil.getResponseFromHttpUrl(url);
                Log.d(TAG, "async task retorno: " + json);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return json;
        }

        @Override
        protected void onPreExecute() {
            mostrarLoading();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            esconderLoading();
            if(s == null){
                tvTextoExibido.setText("Ocorreu um erro");
            } else {
                String j = "{\"descricao\":\"REALIZAMOS TODOS OS EXAMES LABORATORIAIS.\",\"foto\":\"Logo_ArnaldoGRD.jpg\",\"uniq_id\":\"a2163266e92b84644aaee0749d833378\",\"id_estabelecimento\":\"106\",\"razao_social\":\"LABORAT\\u00d3RIO DE AN\\u00c1LISES CL\\u00cdNICAS ARNALDO OLIVEIRA LTDA.\",\"nome_fantasia\":\"LABORAT\\u00d3RIO DE AN\\u00c1LISES CL\\u00cdNICAS - ARNALDO OLIVEIRA - UNID. 3\",\"cnpj_cpf\":\"15790215000120\",\"contato\":\"RECEP\\u00c7\\u00c3O \\/ 929251-1515 \\/ (92) 3649-4000  \",\"telefone\":\"AGENDAMENTO FEITO PELA EVO, LIGUE: 0800 042 0405\",\"celular\":\"\",\"email\":\"FATURANETOLCAO@GMAIL.COM\",\"endereco\":\"AV. PROFESSOR FELIX VALOIS (ATENDE NO M\\u00c9DICOS E IMAGENS)\",\"numero\":\"768\",\"bairro\":\"CIDADE NOVA I\",\"id_cidade\":\"1\",\"id_estado\":\"1\",\"cep\":\"69090-000\",\"nmcidade\":\"MANAUS\",\"nmestado\":\"AMAZONAS\",\"pais\":\"Brasil\",\"status\":\"1\",\"segmento\":\"LABORAT\\u00d3RIOS\",\"beneficios\":\">>>>>>>>>>>\\r\\n NESSA UNIDADE VOC\\u00ca REALIZA SERVI\\u00c7OS GRATUITOS COM O CART\\u00c3O EVO!!!!! \\r\\n >>>>>>>>>>>>>>>>>>>>>>\\r\\n BASTA VOC\\u00ca AGENDAR SEU SERVI\\u00c7O PELA (CENTRAL DE ATENDIMENTO EVO NO: 0800 042 0405)\",\"total_likes\":\"0\"},{\"descricao\":\"REALIZAMOS TODOS OS TIPOS DE EXAMES LABORATORIAIS.\",\"foto\":\"Logo_ArnaldoGRD.jpg\",\"uniq_id\":\"1e0322b527fed032aa627d5469610c4f\",\"id_estabelecimento\":\"107\",\"razao_social\":\"LABORAT\\u00d3RIO DE AN\\u00c1LISES CL\\u00cdNICAS ARNALDO OLIVEIRA LTDA - UNIDADE 1 \",\"nome_fantasia\":\"LABORAT\\u00d3RIO DE AN\\u00c1LISES CL\\u00cdNICAS - ARNALDO OLIVEIRA - UNID. 1\",\"cnpj_cpf\":\"15790215000120\",\"contato\":\"RECEP\\u00c7\\u00c3O \\/ 9221013045 \\/ 9292511515 \",\"telefone\":\"AGENDAMENTO FEITO PELA EVO, LIGUE: 0800 042 0405\",\"celular\":\"\",\"email\":\"FATURANETOLCAO@GMAIL.COM\",\"endereco\":\"RUA MANICOR\\u00c9 (ATENDE NO HOSPITAL SANTO ALBERTO)\",\"numero\":\"536\",\"bairro\":\"CACHOEIRINHA\",\"id_cidade\":\"1\",\"id_estado\":\"1\",\"cep\":\"69035-100\",\"nmcidade\":\"MANAUS\",\"nmestado\":\"AMAZONAS\",\"pais\":\"Brasil\",\"status\":\"1\",\"segmento\":\"LABORAT\\u00d3RIOS\",\"beneficios\":\">>>>>>>>>>>\\r\\n NESSA UNIDADE VOC\\u00ca REALIZA SERVI\\u00c7OS GRATUITOS COM O CART\\u00c3O EVO!!!!! \\r\\n >>>>>>>>>>>>>>>>>>>>>>\\r\\n BASTA VOC\\u00ca AGENDAR SEU SERVI\\u00c7O PELA (CENTRAL DE ATENDIMENTO EVO NO: 0800 042 0405)\",\"total_likes\":\"0\"}";
                Gson gson = new Gson();
                JsonReader reader = new JsonReader(new StringReader(j));
                reader.setLenient(true);
                Informacoes informacoes = gson.fromJson(reader, Informacoes.class);
                Log.d(TAG, "retorno: " + informacoes.razao_social.toString());


                tvTextoExibido.setText(s);
            }
        }
    }


}
